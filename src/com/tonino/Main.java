package com.tonino;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        User person1 = new User("John", "Doe", 9759024002L);
        User person2 = new User("Jane", "Doe", 9348782727L);

        System.out.println("** Person 1 Details **");
        System.out.print("Name: " + person1.userName() + "\n");
        System.out.print("Contact Number: " + person1.userContactNumber() + "\n");

        System.out.println("\n** Person 2 Details **");
        System.out.print("Name: " + person2.userName() + "\n");
        System.out.print("Contact Number: " + person2.userContactNumber() + "\n");

        ArrayList<String> usersList = new ArrayList<String>();
        usersList.add(person1.userName());
        usersList.add(person2.userName());

        System.out.println("\n** Users List **");
        for(String name: usersList) {
            System.out.println(name);
        }
    }
}
