package com.tonino;


public class User {
    private String firstName;
    private String lastName;
    private long contactNumber;

    public User(String newFirstName, String newLastName, long newContactNumber) {
        this.firstName = newFirstName;
        this.lastName = newLastName;
        this.contactNumber = newContactNumber;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public long getContactNumber() {
        return this.contactNumber;
    }

    public String userName() {
        return this.firstName + " " + this.lastName;
    }

    public long userContactNumber() {
        return this.contactNumber;
    }
}

